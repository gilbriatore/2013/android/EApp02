package br.up.edu.eapp02;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void somar(View v) {

        //Buscando as caixas de texto;
        EditText cxDeTxtNumero1 = (EditText) findViewById(R.id.caixaNumero1);
        EditText cxDeTxtNumero2 = (EditText) findViewById(R.id.caixaNumero2);
        EditText cxDeTxtResultado = (EditText) findViewById(R.id.caixaResultado);

        //Carregando o texto das caixas;
        String txtDaCaixa1 = cxDeTxtNumero1.getText().toString();
        String txtDaCaixa2 = cxDeTxtNumero2.getText().toString();

        //Convertendo os números para inteiro;
        int vlrNumero1 = Integer.parseInt(txtDaCaixa1);
        int vlrNumero2 = Integer.parseInt(txtDaCaixa2);

        //Realiza a operação matemática;
        int resultado = vlrNumero1 + vlrNumero2;

        //Converte o número para texto e coloca na caixa;
        cxDeTxtResultado.setText(String.valueOf(resultado));

    }
}











